<?php

namespace AppBundle\Lib\Bank;

use Guzzle\Service\Client as GuzzleClient;

class Client
{
    protected $lastRequestError = null;
    protected $lastRequestCommission = null;
    protected $serviceUrl = null;

    public function __construct($serviceUrl)
    {
        $this->serviceUrl = $serviceUrl;
    }

    public function getLastRequestCommission()
    {
        return $this->lastRequestCommission;
    }

    public function getLastRequestError()
    {
        return $this->lastRequestError;
    }

    public function transaction($senderCard, $recipientCard, $amount)
    {
        $client = new GuzzleClient($this->serviceUrl);
        $request = $client->createRequest('POST', null, null, [
            'senderCard' => $senderCard,
            'recipientCard' => $recipientCard,
            'amount' => $amount,
        ]);
        $response = $request->send();
        $result = json_decode($response->getBody(true));

        if (isset($result->error)) {
            $this->lastRequestError = $result->error;
        }

        if (isset($result->commission)) {
            $this->lastRequestCommission = $result->commission;
        }

        return (bool)$result->status;
    }
}