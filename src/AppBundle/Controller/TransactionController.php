<?php

namespace AppBundle\Controller;

use AppBundle\Form\TransactionType;
use AppBundle\Lib\Bank\Client;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class TransactionController extends Controller
{

    public function doAction(Request $request)
    {
        $form = $this->createForm(new TransactionType(), [], [
            'action' => $this->generateUrl('app_default_index'),
            'method' => 'POST',
        ]);

        $form->handleRequest($request);

        $error = null;
        $commission = null;

        if ($form->isValid()) {
            $client = new Client('http://' . $request->getHost() . $this->generateUrl('bank_transaction_do'));

            $formData = $form->getData();

            if ($client->transaction($formData['senderCard'], $formData['recipientCard'], $formData['amount'])) {
                $commission = $client->getLastRequestCommission();
            } else {
                $error = $client->getLastRequestError();
            }

        } else {
            $error = 'Данные формы невалидны.';
        }

        return $this->render('AppBundle:Transaction:do.html.twig', [
            'form' => $form->createView(),
            'error' => $error,
            'commission' => $commission,
        ]);
    }
}
