<?php

namespace AppBundle\Controller;

use AppBundle\Form\TransactionType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        $form = $this->createForm(new TransactionType(), [], [
            'action' => $this->generateUrl('app_transaction_do'),
            'method' => 'POST',
        ]);

        return $this->render('AppBundle:Default:index.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
