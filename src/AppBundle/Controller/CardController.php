<?php

namespace AppBundle\Controller;

use Inacho\CreditCard;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

class CardController extends Controller
{
    public function validationAction()
    {
        try {
            $request = $this->get('request');
            $validator = new CreditCard();

            return new JsonResponse([
                'success' => $validator->validCreditCard($request->get('card'))
            ]);

        } catch (\Exception $e) {
            return new JsonResponse([
                'fail' => 'Ошибка при попытке проверить карту.'
            ]);
        }
    }


}
