<?php
namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;


class TransactionType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('senderCard')
            ->add('recipientCard')
            ->add('amount')
            ->add('save', 'submit')
        ;
    }


    /**
     * @return string
     */
    public function getName()
    {
        return 'app_transaction';
    }
}
