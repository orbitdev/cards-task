<?php

namespace BankBundle\Controller;

use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\Controller\FOSRestController;
use Inacho\CreditCard;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;

class TransactionController extends FOSRestController
{
    const MIN_AMOUNT = 100;
    const COMMISSION_PERCENT = 2.5;
    const MINIMAL_COMMISSION_FIXED = 50;

    /**
     * @param Request $request the request object
     *
     * @Rest\View
     * @return array
     */
    public function doAction(Request $request)
    {
        if ($request->get('amount') < self::MIN_AMOUNT) {
            return [
                'status' => 0,
                'error' => 'Сумма перевода должна быть не менее ' . self::MIN_AMOUNT . ' рублей',
            ];
        }

        $validator = new CreditCard();
        $cardInfo = $validator->validCreditCard($request->get('senderCard'));
        if (!$cardInfo['valid']) {
            return [
                'status' => 0,
                'error' => 'Карта отправителя невалидна',
            ];
        }

        $cardInfo = $validator->validCreditCard($request->get('recipientCard'));
        if (!$cardInfo['valid']) {
            return [
                'status' => 0,
                'error' => 'Карта получателя невалидна',
            ];
        }

        if (rand(0, 1)) {
            $commission = $request->get('amount') / 100 * 2.5;
            if ($commission < self::MINIMAL_COMMISSION_FIXED) {
                $commission = self::MINIMAL_COMMISSION_FIXED;
            }
            return [
                'status' => 1,
                'commission' => $commission,
            ];
        } else {
            return [
                'status' => 0,
                'error' => 'Не хватает средств на карте'
            ];
        }
    }
}
