$(function() {

    function checkCard(card, successCallback, failCallback) {
        $.post(window.app.routes.app_default_card_validation, {card: card}, function(response) {
            console.log(response);
            if (response.success) {
                if (response.success.valid) {
                    successCallback(response.success);
                } else {
                    failCallback('Карта не валидна');
                }
            } else if (response.fail) {
                failCallback(response.fail);
            } else {

                failCallback('Неизвестная ошибка.');
            }
        });
    }

    $('.j-input-sender-card').bind('blur', function() {
        return checkCard(
            $(this).val(),
            function(data) {
                var icon = window.app.icons.cards[data.type];
                var path = window.app.routes.img_path;
                $('.j-sender-card-icon').html('<img src="'+path+icon+'" style="height: 20px;">');
                $('.j-sender-card-status').html('Ok!').addClass('green').removeClass('red');
            },
            function(error) {
                $('.j-sender-card-status').html(error).addClass('red').removeClass('green');
            }
        );
    });

    $('.j-input-recipient-card').bind('blur', function() {
        return checkCard(
            $(this).val(),
            function(data) {
                var icon = window.app.icons.cards[data.type];
                var path = window.app.routes.img_path;
                $('.j-recipient-card-icon').html('<img src="'+path+icon+'" style="height: 20px;">');
                $('.j-recipient-card-status').html('Ok!').addClass('green').removeClass('red');
            },
            function(error) {
                $('.j-recipient-card-status').html(error).addClass('red').removeClass('green');
            }
        );
    });

    $('.j-input-amount').bind('blur', function() {
        var amount = parseInt($(this).val());
        if (isNaN(amount) || amount < window.app.params.minAmount) {
            $('.j-amount-status').html('Сумма должна быть не менее ' + window.app.params.minAmount + ' рублей').addClass('red').removeClass('green');
        } else {
            $('.j-amount-status').html('Ok!').addClass('green').removeClass('red');
        }
    });

    $('.j-form').bind('submit', function() {
        if ($('.red:visible').length > 0) {
            alert('Пожалуйста, проверьте правильность заполнения формы!');
            return false;
        }
    });
});